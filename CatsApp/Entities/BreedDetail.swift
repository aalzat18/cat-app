//
//  BreedDetail.swift
//  CatsApp
//
//  Created by Alejandro  Alzate on 12/9/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
struct BreedDetail: Codable {
    var breeds: [Breed]
    var imageUrl: String
    
    enum CodingKeys: String,CodingKey {
        case breeds, imageUrl = "url"
    }
}
