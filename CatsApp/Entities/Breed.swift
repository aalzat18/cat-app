//
//  Breed.swift
//  CatsApp
//
//  Created by Alejandro  Alzate on 12/8/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
struct Breed: Codable {
    var id: String
    var name: String
    var temperament: String?
    var origin: String?
    var countryCode: String?
    var description: String?
    var wikipediaurl: String?
    
    enum CodingKeys: String,CodingKey {
        case id,name,origin,temperament,description,wikipediaurl = "wikipedia_url"
        case countryCode = "country_code"
    }

}
