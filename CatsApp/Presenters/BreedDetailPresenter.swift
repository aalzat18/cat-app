//
//  BreedDetailPresenter.swift
//  CatsApp
//
//  Created by Alejandro  Alzate on 12/8/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation

class BreedDetailPresenter  {
    
    // MARK: Properties
    weak var view: BreedDetailViewProtocol?
    var interactor: BreedDetailInteractorInputProtocol?
    var wireFrame: BreedDetailWireFrameProtocol?
    var breed: Breed?
}

extension BreedDetailPresenter: BreedDetailPresenterProtocol {
    // TODO: implement presenter methods
    func viewDidLoad() {
        interactor?.getData(data: self.breed!)
    }
}

extension BreedDetailPresenter: BreedDetailInteractorOutputProtocol {
    
    func interactorCallBackData(breedDetail: BreedDetail) {
        view?.presenterCallBackData(breedDetail: breedDetail)
    }
    
    func errorRemote(error: Error) {
        view?.errorRemote(error: error)
    }
}
