//
//  MainPresenter.swift
//  CatsApp
//
//  Created by Alejandro  Alzate on 12/8/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation

class MainPresenter  {
    
    weak var view: MainViewProtocol?
    var interactor: MainInteractorInputProtocol?
    var wireFrame: MainWireFrameProtocol?
}

extension MainPresenter: MainPresenterProtocol {
    
    func viewDidLoad() {
        interactor?.getData()
    }
    
    func showDetailView(with breed: Breed) {
        wireFrame?.presentNewViewDetail(from: view!, withData: breed)
    }
}

extension MainPresenter: MainInteractorOutputProtocol {
    
    func interactorCallBackData(result breeds: [Breed]) {
        view?.presenterCallBackData(result: breeds)
    }
    
    func errorRemote(error: Error) {
        view?.errorRemote(error: error)
    }
}
