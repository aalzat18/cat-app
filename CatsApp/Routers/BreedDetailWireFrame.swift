//
//  BreedDetailWireFrame.swift
//  CatsApp
//
//  Created by Alejandro  Alzate on 12/8/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
import UIKit

class BreedDetailWireFrame: BreedDetailWireFrameProtocol {
   
    class func createBreedDetailModule(with data: Breed) -> UIViewController {
        let navController = mainStoryboard
        if let view = navController as? BreedDetailView {
            let presenter: BreedDetailPresenterProtocol & BreedDetailInteractorOutputProtocol = BreedDetailPresenter()
            let interactor: BreedDetailInteractorInputProtocol & BreedDetailRemoteDataManagerOutputProtocol = BreedDetailInteractor()
            let remoteDataManager: BreedDetailRemoteDataManagerInputProtocol = DetailRepository()
            let wireFrame: BreedDetailWireFrameProtocol = BreedDetailWireFrame()
            
            view.presenter = presenter
            presenter.view = view
            presenter.wireFrame = wireFrame
            presenter.interactor = interactor
            presenter.breed = data
            interactor.presenter = presenter
            interactor.remoteDatamanager = remoteDataManager
            remoteDataManager.remoteRequestHandler = interactor
            
            return navController
        }
        return UIViewController()
    }
    
    static var mainStoryboard: UIViewController {
        return BreedDetailView(nibName: "BreedDetail", bundle: Bundle.main)
    }
}
