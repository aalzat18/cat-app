//
//  BreedDetailView.swift
//  CatsApp
//
//  Created by Alejandro  Alzate on 12/8/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
import UIKit

class BreedDetailView: UIViewController {

    @IBOutlet weak var breedImageView: UIImageView!
    @IBOutlet weak var breedNameLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var temperamentLabel: UILabel!
    
    // MARK: Properties
    var presenter: BreedDetailPresenterProtocol?
    var detail: BreedDetail?

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        let play = UIBarButtonItem(title: "More Info", style: .plain, target: self, action: #selector(wikiTouch))
        navigationItem.rightBarButtonItems = [ play]
        presenter?.viewDidLoad()
    }
    
    @IBAction func wikiTouch(_ sender: UIButton) {
        if let url = URL(string: detail!.breeds[0].wikipediaurl!) {
           UIApplication.shared.open(url)
       }
    }
}

extension BreedDetailView: BreedDetailViewProtocol {
    func presenterCallBackData(breedDetail: BreedDetail) {
        DispatchQueue.main.async {
            self.detail = breedDetail
            self.breedNameLabel.text = breedDetail.breeds[0].name
            self.countryLabel.text = "\(breedDetail.breeds[0].origin!) - \(breedDetail.breeds[0].countryCode!)"
            self.descriptionLabel.text = breedDetail.breeds[0].description
            self.temperamentLabel.text = breedDetail.breeds[0].temperament
            self.breedImageView.downloadImage(from: URL(string: breedDetail.imageUrl)!)
        }
    }
    
    func errorRemote(error: Error) {
        DispatchQueue.main.async {
            let alertDialog = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            alertDialog.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertDialog, animated: true, completion: nil)
        }
    }
}

extension UIImageView {
    
   func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
      URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
   }
    
   func downloadImage(from url: URL) {
      getData(from: url) {
         data, response, error in
         guard let data = data, error == nil else {
            return
         }
         DispatchQueue.main.async() {
            self.image = UIImage(data: data)
         }
      }
   }
}
