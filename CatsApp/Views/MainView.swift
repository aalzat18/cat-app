//
//  MainView.swift
//  CatsApp
//
//  Created by Alejandro  Alzate on 12/8/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
import UIKit

class MainView: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var catListTableView: UITableView!
    // MARK: Properties
    var presenter: MainPresenterProtocol?
    var breedList = [Breed]()

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.catListTableView.isAccessibilityElement = true
        presenter?.viewDidLoad()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return breedList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        cell.textLabel?.text = breedList[indexPath.row].name
        cell.accessoryType = .disclosureIndicator
        cell.isAccessibilityElement = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.showDetailView(with: breedList[indexPath.row])
    }
}

extension MainView: MainViewProtocol {
    
    func presenterCallBackData(result: [Breed]) {
        DispatchQueue.main.async {
            self.breedList = result
            self.catListTableView.reloadData()
        }
    }
    
    func errorRemote(error: Error) {
        DispatchQueue.main.async {
            let alertDialog = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            alertDialog.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertDialog, animated: true, completion: nil)
        }
    }
}
