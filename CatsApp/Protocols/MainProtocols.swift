//
//  MainProtocols.swift
//  CatsApp
//
//  Created by Alejandro  Alzate on 12/8/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
import UIKit

protocol MainViewProtocol: class {
    // PRESENTER -> VIEW
    var presenter: MainPresenterProtocol? { get set }
    
    func presenterCallBackData(result: [Breed])
    func errorRemote(error : Error)
}

protocol MainWireFrameProtocol: class {
    // PRESENTER -> WIREFRAME
    static func createMainModule() -> UIViewController
    func presentNewViewDetail(from view : MainViewProtocol, withData: Breed)
}

protocol MainPresenterProtocol: class {
    // VIEW -> PRESENTER
    var view: MainViewProtocol? { get set }
    var interactor: MainInteractorInputProtocol? { get set }
    var wireFrame: MainWireFrameProtocol? { get set }
    
    func viewDidLoad()
    func showDetailView(with breed: Breed)
}

protocol MainInteractorOutputProtocol: class {
    // INTERACTOR -> PRESENTER
    func interactorCallBackData(result breeds: [Breed])
    func errorRemote(error : Error)
}

protocol MainInteractorInputProtocol: class {
    // PRESENTER -> INTERACTOR
    var presenter: MainInteractorOutputProtocol? { get set }
    var remoteDatamanager: MainRemoteDataManagerInputProtocol? { get set }
    
    func getData()
}

protocol MainDataManagerInputProtocol: class {
    // INTERACTOR -> DATAMANAGER
}

protocol MainRemoteDataManagerInputProtocol: class {
    // INTERACTOR -> REMOTEDATAMANAGER
    var remoteRequestHandler: MainRemoteDataManagerOutputProtocol? { get set }
    
    func getData()
}

protocol MainRemoteDataManagerOutputProtocol: class {
    // REMOTEDATAMANAGER -> INTERACTOR
    func remoteDataManagerCallBackData(result breeds: [Breed])
    func errorRemote(error : Error)
}
