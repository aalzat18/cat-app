//
//  BreedDetailProtocols.swift
//  CatsApp
//
//  Created by Alejandro  Alzate on 12/8/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
import UIKit

protocol BreedDetailViewProtocol: class {
    // PRESENTER -> VIEW
    var presenter: BreedDetailPresenterProtocol? { get set }
    
    func presenterCallBackData(breedDetail: BreedDetail)
    func errorRemote(error : Error)
}

protocol BreedDetailWireFrameProtocol: class {
    // PRESENTER -> WIREFRAME
    static func createBreedDetailModule(with data : Breed) -> UIViewController
}

protocol BreedDetailPresenterProtocol: class {
    // VIEW -> PRESENTER
    var view: BreedDetailViewProtocol? { get set }
    var interactor: BreedDetailInteractorInputProtocol? { get set }
    var wireFrame: BreedDetailWireFrameProtocol? { get set }
    var breed: Breed? { get set }
    
    func viewDidLoad()
}

protocol BreedDetailInteractorOutputProtocol: class {
// INTERACTOR -> PRESENTER
    func interactorCallBackData(breedDetail: BreedDetail)
    func errorRemote(error : Error)
}

protocol BreedDetailInteractorInputProtocol: class {
    // PRESENTER -> INTERACTOR
    var presenter: BreedDetailInteractorOutputProtocol? { get set }
    var remoteDatamanager: BreedDetailRemoteDataManagerInputProtocol? { get set }
    
    func getData(data: Breed)
}

protocol BreedDetailDataManagerInputProtocol: class {
    // INTERACTOR -> DATAMANAGER
}

protocol BreedDetailRemoteDataManagerInputProtocol: class {
    // INTERACTOR -> REMOTEDATAMANAGER
    var remoteRequestHandler: BreedDetailRemoteDataManagerOutputProtocol? { get set }
    
    func getData(data: Breed)
}

protocol BreedDetailRemoteDataManagerOutputProtocol: class {
    // REMOTEDATAMANAGER -> INTERACTOR
    func remoteDataManagerCallBackData(result breedDetail: BreedDetail)
    func errorRemote(error : Error)
}
