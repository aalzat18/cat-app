//
//  BaseRepository.swift
//  CatsApp
//
//  Created by Alejandro  Alzate on 12/8/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
class BaseRepository: NSObject {
    lazy var endPoint = EndPoints()
    lazy var constant = ApiConstants()
}
