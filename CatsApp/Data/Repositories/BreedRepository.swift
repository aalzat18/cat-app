//
//  BreedRepository.swift
//  CatsApp
//
//  Created by Alejandro  Alzate on 12/8/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
class BreedRepository: BaseRepository, MainRemoteDataManagerInputProtocol {
    
    var remoteRequestHandler: MainRemoteDataManagerOutputProtocol?
    
    func getData() {
        let url = endPoint.baseUrl + endPoint.breedListEndPoint
        let session = URLSession.shared
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = constant.getMethod
        request.setValue(constant.config, forHTTPHeaderField: constant.contentType)
        
        session.dataTask(with: request){(data,response,error) in
            guard let data = data,error == nil , let result = response as?
                HTTPURLResponse else{
                    self.remoteRequestHandler?.errorRemote(error: error!)
                    return
            }
            
            if(result.statusCode == 200){
                do{
                
                 let decoder = JSONDecoder()
                 let breed = try decoder.decode([Breed].self, from: data);
                 self.remoteRequestHandler?.remoteDataManagerCallBackData(result: breed)
              
                }catch{
                 self.remoteRequestHandler?.errorRemote(error: error)
                }
            }else{
             
             self.remoteRequestHandler?.errorRemote(error: error!)
            }
            
        }.resume()
    }
}
