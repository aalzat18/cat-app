//
//  DetailRepository.swift
//  CatsApp
//
//  Created by Alejandro  Alzate on 12/9/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
class DetailRepository: BaseRepository, BreedDetailRemoteDataManagerInputProtocol {
    
    var remoteRequestHandler: BreedDetailRemoteDataManagerOutputProtocol?
    
    func getData(data: Breed) {
        let detailEndPoint = String(format: endPoint.fetchBreedByIdEndPoint, data.id)
        let url = endPoint.baseUrl + detailEndPoint
        let session = URLSession.shared
        var request = URLRequest.init(url: URL(string: url)!)
        request.httpMethod = constant.getMethod
        request.setValue(constant.config, forHTTPHeaderField: constant.contentType)
        session.dataTask(with: request){(data, response, error) in
            guard let data = data, error == nil, let result = response as? HTTPURLResponse else{
                self.remoteRequestHandler?.errorRemote(error: error!)
                return
            }
            
            if(result.statusCode == 200){
                do{
                    let decoder = JSONDecoder()
                    let breed = try decoder.decode([BreedDetail].self, from: data);
                    self.remoteRequestHandler?.remoteDataManagerCallBackData(result: breed[0])
                }catch{
                    self.remoteRequestHandler?.errorRemote(error: error)
                }
            }else{
                self.remoteRequestHandler?.errorRemote(error: error!)
            }
        }.resume()
    }
}
