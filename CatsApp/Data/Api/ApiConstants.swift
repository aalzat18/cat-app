//
//  ApiConstants.swift
//  CatsApp
//
//  Created by Alejandro  Alzate on 12/8/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
class ApiConstants: NSObject {
    let getMethod = "Get"
    let contentType = "Content-Type"
    let config = "application/json; charset=utf8"
}
