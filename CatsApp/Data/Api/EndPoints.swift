//
//  EndPoints.swift
//  CatsApp
//
//  Created by Alejandro  Alzate on 12/8/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
class EndPoints{
    let baseUrl = "https://api.thecatapi.com/v1/"
    let breedListEndPoint = "breeds"
    let fetchBreedByIdEndPoint  = "images/search?breed_ids=%@"
    let breedImageEndPoint = "images/search"    
}
