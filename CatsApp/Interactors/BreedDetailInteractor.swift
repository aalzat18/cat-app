//
//  BreedDetailInteractor.swift
//  CatsApp
//
//  Created by Alejandro  Alzate on 12/8/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation

class BreedDetailInteractor: BreedDetailInteractorInputProtocol {
    
    // MARK: Properties
    weak var presenter: BreedDetailInteractorOutputProtocol?
    var remoteDatamanager: BreedDetailRemoteDataManagerInputProtocol?
    
    func getData(data: Breed) {
        remoteDatamanager?.getData(data: data)
    }
}

extension BreedDetailInteractor: BreedDetailRemoteDataManagerOutputProtocol {
    
    func remoteDataManagerCallBackData(result breedDetail: BreedDetail) {
        presenter?.interactorCallBackData(breedDetail: breedDetail)
    }
    
    func errorRemote(error: Error) {
        presenter?.errorRemote(error: error)
    }
}
