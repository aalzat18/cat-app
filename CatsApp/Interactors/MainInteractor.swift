//
//  MainInteractor.swift
//  CatsApp
//
//  Created by Alejandro  Alzate on 12/8/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation

class MainInteractor: MainInteractorInputProtocol {
    
    // MARK: Properties
    weak var presenter: MainInteractorOutputProtocol?
    var remoteDatamanager: MainRemoteDataManagerInputProtocol?
    
    func getData() {
        remoteDatamanager?.getData()
    }
}

extension MainInteractor: MainRemoteDataManagerOutputProtocol {
    
    func remoteDataManagerCallBackData(result breeds: [Breed]) {
        presenter?.interactorCallBackData(result: breeds)
    }
    
    func errorRemote(error: Error) {
        presenter?.errorRemote(error: error)
    }
}
